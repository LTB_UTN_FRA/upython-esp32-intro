# ESP32 Intro

## Grabar la imagen de micro python

Los pasos iniciales están tomados de el [este tutorial](https://docs.micropython.org/en/latest/esp32/tutorial/intro.html).

Primero es necesario poner una imagen en el esp32 con el micro python base.
Se descarga de [aquí](https://micropython.org/download#esp32) la ultima versión para el esp32. En este repositorio, se deja en `img` la ultima imagen.

Luego en el host, se corre:

```
pip install esptool

esptool.py --port /dev/ttyUSB0 erase_flash

esptool.py --chip esp32 --port /dev/ttyUSB0 write_flash -z 0x1000 esp32-20180511-v1.9.4.bin

```

Si el sistema booteo correctamente, tenemos acceso a un prompt de python por una interfaz serie, donde ya podemos escribir python!

Para esto es necesario tener una consola a través de la UART (i.e: puty, minicom, screen, xterm, etc), con un baudrate de 115200 y 8N1.

```
minicom -D /dev/ttyUSB0 -b 115200
>>> print("Habemus python!")
Habemus python!
```

De aquí en mas, la terminal por UART es un elemento útil para ver el log de booteo del esp32 o cualquier otra cosa (siempre y cuando no haya
otra herramienta queriendo usarlo).

## Herramientas de desarrollo con el esp32+micro python

### RSHELL

(Remote micropyton console)

Sirve para poder compartir archivos/scripts entre el esp32 y el host de forma simple.
A su vez, desde aquí se puede llamar al prompt de python.

Instalar:
```
pip install rshell
```

Correr:
```
rshell -p /dev/ttyUSB0 -b 115200 --quiet
```

Con eso ultimo se entra al shell compartido con el esp32, el cual queda montado en `/pyboard`

Dentro de la consola compartida, si se quiere copiar una app al esp32, se hace:

```
cp examples/blinky.py /pyboard
```

Para levantar el prompt se corre:

```
repl
```

Si uno quiere levantar el script que recien se corrio, simplemente en el prompt se hace

```
import blinky
```

(CTRL+X para salir del prompt y CTRL+C para salir del shell compartido)

### AMPY

(Adafruit MicroPython tool)

Queda medio redundante teniendo el rshell, pero es util para dejar la aplicación fija de la placa, ya que copia todo lo que se requiera al esp32 y queda
como app booteable.

Instalar:
```
pip install adafruit-ampy
```

Poner un script como app principal:
```
ampy --port /dev/ttyUSB0 put examples/ledwifi.py /main.py
ampy --port /dev/ttyUSB0 put boot.py /boot.py
```

El archivo `boot.py` corre antes que el `main.py` y en el mismo están incluidas las funciones para conectarse a una red wifi estática (pasandole SSID y password).