import socket
from machine import Pin
import network
import time

import esp

yourWifiSSID = "PONER EL NOMBRE DEL SSID DE LA RED ACÄ"
yourWifiPassword = "PONER PASSWORD ACÄ"

def init():
    connect(SSID=yourWifiSSID, Password=yourWifiPassword)
    print("Active lan")

def setup_led(number):
    led = Pin(number, Pin.OUT)
    return led

def web_page(led):
    if led.value()==1:
        led_state="PRENDIDO"
    else:
        led_state="APAGADO"
    html = """<html><head> <title>TEST</title> <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="data:,"> <style>html{font-family: Helvetica; display:inline-block; margin: 0px auto; text-align: center;}
    h1{color: #0F3376; padding: 2vh;}p{font-size: 1.5rem;}.button{display: inline-block; background-color: #e7bd3b; border: none; 
    border-radius: 4px; color: white; padding: 16px 40px; text-decoration: none; font-size: 30px; margin: 2px; cursor: pointer;}
    .button2{background-color: #4286f4;}</style></head><body> <h1>TEST led</h1> 
    <p>LED: <strong>""" + led_state + """</strong></p><p><a href="/?led=on"><button class="button">PRENDER</button></a></p>
    <p><a href="/?led=off"><button class="button button2">APAGAR</button></a></p></body></html>"""
    return html

if __name__ == "__main__":
    init()
    led = Pin(25, Pin.OUT)
    led.value(1)
    print("start main")
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    print("init socket")
    s.bind(('', 80))
    print("init blind")
    s.listen(5)
    print("init listen")
    print("config socket")

    while True:
        conn, addr = s.accept()
        print('Got a connection from %s' % str(addr))
        request = conn.recv(1024)
        request = str(request)
        print('Content = %s' % request)
        led_on = request.find('/?led=on')
        led_off = request.find('/?led=off')
        if led_on == 6:
            print('Led prendido')
            led.value(1)
        if led_off == 6:
            print('Led apagado')
            led.value(0)
        response = web_page(led)
        conn.send(response)
        conn.close()